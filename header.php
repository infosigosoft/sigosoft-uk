<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168040639-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-168040639-1');
    </script>

        <!-- preloader begin -->
        <div class="preloader">
            <img src="assets/img/logo-sigosoft.png" alt="Sigosoft Logo" />
        </div>
        <!-- preloader end -->    
        <!-- header begin -->
        <div class="header-2">
            <div class="topbar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                            <p class="welcome-text">We provide worldwide services 24/7</p>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="support-bars">
                                <ul>
                                    <li>
                                        <span class="icon"><i class="fas fa-envelope-open"></i></span>
                                        info@sigosoft.com
                                    </li>
                                    <li>
                                        <span class="icon"><i class="fas fa-phone"></i></span>
                                         +91 9846237384
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottombar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            
                            <div class="mainmenu">
                                <nav class="navbar navbar-expand-lg">
                                    <a class="navbar-brand" href="."><img src="assets/img/logo-sigosoft.png" alt="Sigosoft Logo"></a>
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav ml-auto">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            About
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                        <a class="dropdown-item" href="about">Our Profile</a>
                                                        <a class="dropdown-item" href="team">Our Team</a>                                                   
                                                        <a class="dropdown-item" href="technologies">Our Technologies</a>
                                                        <a class="dropdown-item" href="partner-with-us">Partner with Us</a>
                                                    
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Services
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                                    <a class="dropdown-item" href="android-app-development-company-in-uk">Android Development</a>
                                                    <a class="dropdown-item" href="ios-app-development-company-in-uk">iOS Development</a>
                                                    <a class="dropdown-item" href="Flutter-app-development-company-in-uk">Cross Platform Development</a>
                                                    <a class="dropdown-item" href="corporate-website-development-company-in-uk">Corporate Website Development</a>
                                                    <a class="dropdown-item" href="eCommerce-website-development-company-in-uk">E-Commerce Development</a>
                                                    <a class="dropdown-item" href="magento-development-company-in-uk">Magento Development</a>
                                                    <a class="dropdown-item" href="wordpress-development-company-in-uk">CMS  Development</a>
                                                    <a class="dropdown-item" href="digital-marketing-company-in-uk">Digital Marketing</a>
                                                    <a class="dropdown-item" href="seo-company-in-uk">Search Engine Optimization</a>
                                                    <a class="dropdown-item" href="social-media-marketing-company-in-uk">Social Media Marketing</a>
                                                        
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Products
                                                </a>
                                                <div class="dropdown-menu dropdown-double" aria-labelledby="navbarDropdown4">
                                                   <div class="row dropdown-row">
                                                      <div class="col-lg-6 dropdown-col">
                                                         <div>   
                                                           <a class="dropdown-item" href="loyalty-mobile-app-development-company-in-uk">Loyalty Apps</a>    
                                                           <a class="dropdown-item" href="eCommerce-web-and-mobile-apps-development-company-in-uk">E-Commerce Apps</a>

                                                           <a class="dropdown-item" href="supply-chain-mobile-app-development-company-in-uk">Supply Chain Apps</a>
                                                           
                                                           <a class="dropdown-item" href="e-learning-mobile-app-development-company-in-uk">E-Learning Apps</a>

                                                           <a class="dropdown-item" href="community-mobile-app-development-company-in-uk">Community Apps</a>

                                                           <a class="dropdown-item" href="flight-booking-mobile-app-development-company-in-uk">Flight Booking Apps</a>

                                                           <a class="dropdown-item" href="hotel-booking-mobile-app-development-company-in-uk">Hotel Booking Apps</a>

                                                           <a class="dropdown-item" href="telemedicine-mobile-app-development-company-in-uk">Telemedicine Apps</a>
                                                         </div>
                                                      </div>
                                                      <div class="col-lg-6 dropdown-col">
                                                         <div> 
                                                           <a class="dropdown-item" href="gym-mobile-app-development-company-in-uk">Gym & Fitness Apps</a> 
                                                           
                                                           <a class="dropdown-item" href="rent-a-car-app-development-company-in-uk">Rent a Car Apps</a>

                                                           <!--<a class="dropdown-item" href="tablet-pos-mobile-app-development-company-in-uk">Tablet POS Apps</a>-->

                                                           <a class="dropdown-item" href="sports-booking-mobile-app-development-company-in-uk">Sports Apps</a>
                                                           
                                                           <a class="dropdown-item" href="food-delivery-app-development-company-in-uk">Food Delivery Apps</a>

                                                           <a class="dropdown-item" href="classified-app-development-company-in-uk">Classified Apps</a>
                                                           
                                                           <a class="dropdown-item" href="car-wash-app-development-company-in-uk">Car Wash Apps</a>
                                                           <a class="dropdown-item" href="van-sales-app-development-company-in-uk">Van sales Apps</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                            </li>
                                            <?php /*<li class="nav-item">
                                                <a class="nav-link" href="partner-with-us">Partner with Us</a>
                                            </li> */?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="portfolio">Portfolio</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="blog">Blogs</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="careers">Careers</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="contact">Contact</a>
                                            </li>
                                        </ul>
                                        <div class="header-buttons">
                                            <a href="contact" class="quote-button">Get A Quote</a>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- header end -->