<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIGOSOFT</title>
    <?php include('styles.php'); ?>

        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">

    </head>
    <body>
        <?php include('header.php'); ?>
        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-careers">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>Careers</h2>
                            <ul>
                                <li><a href=".">Home</a></li>
                                <li>Careers</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <div class="thank-you section-bg-black text-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3><i class="fas fa-exclamation-triangle"></i></h3>
                        <h2>Oops!</h2>
                        <p>Something went wrong. Your application could not be posted. Please try again later..</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- service begin -->
        <div class="service service-3 pt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-8">
                        <div class="section-title-2 text-center mb-5">
                            <h2>Our Services</h2>
                            <!--<p>But I must explain to you how all this mistaken denouncing
                                    praising pain was born and I will give you</p>-->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/android-app-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Android App Development
                                    <span class="bg-number">01</span>
                                </h3>
                                <p class="service-content">With us, you can get 100% quality android app development services. Our expert team of android app developers create unique strategies to develop a mobile application for the clients to achieve their business goals.</p>
                                <a href="android-app-development-company-in-uk" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/ios-app-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">iOS App Development
                                    <span class="bg-number">02</span>
                                </h3>
                                <p class="service-content">When it comes to mobile application, iOS apps play a significant role. Now, want to have an iOS app for your business? If yes, then reach us. We are the leading iOS app development company in the UK.</p>
                                <a href="ios-app-development-company-in-uk" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1500" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/flutter-app-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Flutter App Development
                                    <span class="bg-number">03</span>
                                </h3>
                                <p class="service-content">As of now, many companies are in search of Flutter app development services. If you’re one of them, then you can stop your search here. At Sigosoft, you can get top class quality Flutter app development solutions.</p>
                                <a href="Flutter-app-development-company-in-uk" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>


                    <div class="col-xl-4 col-lg-4 col-md-6 offset-lg-2" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/ecommerce-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">E-commerce Development
                                    <span class="bg-number">04</span>
                                </h3>
                                <p class="service-content">Facing difficulty in finding the best eCommerce website development service provider? Don’t struggle more for this. We are here for you. Being the best mobile app development company in UK, London, here you can ensure the best and quality service.</p>
                                <a href="eCommerce-website-development-company-in-uk" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/magento-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Magento Development
                                    <span class="bg-number">05</span>
                                </h3>
                                <p class="service-content">Want a user-friendly, reliable, and secured eCommerce site using Magento platform? We can help you with the best Magento development services at affordable rates.</p>
                                <a href="magento-development-company-in-uk" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <?php /*<div class="col-xl-3 col-lg-3 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1500" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/cms-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">CMS Development
                                    <span class="bg-number">06</span>
                                </h3>
                                <p class="service-content">CMS is one of the best platforms to manage all sorts of contents. At Sigosoft, we rely on the CMS platform to create, manage, or edit the content on the website. Reach us to know more about how we work on the CMS platform.</p>
                                <a href="cms-website-development.php" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1500" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/corporate-website-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Corporate Website Development
                                    <span class="bg-number">07</span>
                                </h3>
                                <p class="service-content">Want a corporate website for your business? We are ready to assist you. Here at Sigosoft, we design and develop corporate websites, which act as a lead generator. With our services, you can enhance your ROI. </p>
                                <a href="corporate-website-development.php" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>*/?>

                </div>
            </div>
        </div>
        <!-- service end -->

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
    </body>


</html>