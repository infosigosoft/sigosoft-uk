        <!-- footer begin -->
        <div class="footer footer-2">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between">
                    
                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>About Us</h3>
                            <ul>
                                <li>
                                    <a href="https://sigosoft.co.uk/about">Our Profile</a>
                                </li>
                                <li>
                                    <a href="https://sigosoft.co.uk/partner-with-us">Partner with Us</a>
                                </li>
                                <li>
                                    <a href="https://sigosoft.co.uk/team">Our Team</a>
                                </li>
                                <li>
                                    <a href="https://sigosoft.co.uk/careers">Careers</a>
                                </li>
                                <!--<li>
                                    <a href="#">Testimonials</a>
                                </li>-->
                                <li>
                                    <a href="https://sigosoft.co.uk/technologies">Technologies</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>Services</h3>
                            <ul>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/android-app-development-company-in-uk">Android Development</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/ios-app-development-company-in-uk">iOS Development</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/cross-platform-app-development-company-in-uk">Cross Platform Development</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/corporate-website-development-company-in-uk">Corporate Website Development</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/eCommerce-website-development-company-in-uk">E-Commerce Development</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/magento-development-company-in-uk">Magento Development</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/content-management-website-development-company-in-uk">CMS  Development</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/digital-marketing-company-in-uk">Digital Marketing</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/seo-company-in-uk">Search Engine Optimization</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/social-media-marketing-company-in-uk">Social Media Marketing</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>Products</h3>
                            <ul>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/loyalty-mobile-app-development-company-in-uk">Loyalty Apps</a>    
                                </li>
                                <li>
                                    <a  href="eCommerce-web-and-mobile-apps-development-company-in-uk">E-Commerce Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/supply-chain-mobile-app-development-company-in-uk">Supply Chain Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/e-learning-mobile-app-development-company-in-uk">E-Learning Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/community-mobile-app-development-company-in-uk">Community Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/flight-booking-mobile-app-development-company-in-uk">Flight Booking Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/hotel-booking-mobile-app-development-company-in-uk">Hotel Booking Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/online-doctor-consultation-app-development-company-in-uk">Online Consultation Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/gym-mobile-app-development-company-in-uk">Gym & Fitness Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/rent-a-car-app-development-company-in-uk">Rent a Car Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/tablet-pos-mobile-app-development-company-in-uk">Tablet POS Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/sports-booking-mobile-app-development-company-in-uk">Sports Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/food-delivery-app-development-company-in-uk">Food Delivery Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/classified-app-development-company-in-uk">Classified Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/car-wash-app-development-company-in-uk">Car Wash Apps</a>
                                </li>
                                <li>
                                    <a  href="https://www.sigosoft.co.uk/van-sales-app-development-company-in-uk">Van sales Apps</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-8">
                        <div class="about-widget links-widget">
                            <h3>Contact</h3>
                            <ul>
                                <li><a href="tel:+971 56 253 0256"><i class="fas fa-mobile-alt"></i>+91 9846237384</a></li>
                                <?php /*<li><a href="https://api.whatsapp.com/send?phone=919846237384"><i class="fab fa-whatsapp"></i> +91 9846237384</a></li> */?>
                                <li><a href="mailto:info@sigosoft.com"><i class="far fa-envelope-open"></i> info@sigosoft.com</a></li>
                                <li style="color: #bdbdbd; font-size: 15px; line-height: 25px;"><i class="fas fa-map-marker-alt"></i>
                                    <span>London, UK</span><br>
                                    
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
         <!-- copyright begin -->
        <div class="copyright">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="cp-area">
                            <p>Copyright © 2014-2020 Sigosoft. All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="social-area">
                            <ul>
                                <li>
                                    <a class="facebook" href= https://www.facebook.com/sigosoft><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a class="instagram" href=https://www.instagram.com/sigosoft_><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="https://twitter.com/sigosoft_social"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="skype" href=https://join.skype.com/invite/Qq1GmNOs9DDV><i class="fab fa-skype"></i></a>
                                </li>
                                 <li>
                                  <a class="facebook" href= https://www.linkedin.com/company/13273886><i class="fab fa-linkedin"></i></a>
                               
                                   
                        <!-- <a class= "linkedin" href= https://www.linkedin.com/company/13273886"><i class="fab fa-linkedin"></i></a>-->
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- copyright end -->

        <div class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=919846237384" target="_blank"><img src="https://sigosoft.co.uk/assets/img/whatsapp.png" alt="whatsapp"/></a>
        </div>
        
        
            <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f022eb4760b2b560e6fc8b8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
            <!-- jquery -->
        <script src="https://sigosoft.co.uk/assets/js/jquery-3.4.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <!-- owl carousel -->
        <script src="https://sigosoft.co.uk/assets/js/owl.carousel.min.js"></script>
        <!-- magnific popup -->
        <script src="https://sigosoft.co.uk/assets/js/jquery.magnific-popup.min.js"></script>
        <!-- counter up js -->
        <script src="https://sigosoft.co.uk/assets/js/jquery.counterup.min.js"></script>
        <script src="https://sigosoft.co.uk/assets/js/counter-us-activate.min.js"></script>
        <!-- waypoints js-->
        <script src="https://sigosoft.co.uk/assets/js/jquery.waypoints.min.js"></script>
        <!-- wow js-->
        <script src="https://sigosoft.co.uk/assets/js/wow.min.js"></script>
        <!-- aos js -->
        <script src="https://sigosoft.co.uk/assets/js/aos.min.js"></script>
        <!-- main -->
        <script src="https://sigosoft.co.uk/assets/js/main.min.js"></script>
        <!--<script src="assets/js/script.js"></script>-->


    
    
        
        
        <!-- footer end -->

       