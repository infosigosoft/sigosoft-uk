<!-- preloader begin -->
        <div class="preloader">
            <img src="https://www.sigosoft.co.uk/assets/img/logo-sigosoft.png" alt="Sigosoft Logo" />
        </div>
        <!-- preloader end -->
<!-- header begin -->
        <div class="header-2">
            <div class="topbar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                            <p class="welcome-text">We provide worldwide services 24/7</p>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="support-bars">
                                <ul>
                                    <li>
                                        <span class="icon"><i class="fas fa-envelope-open"></i></span>
                                        info@sigosoft.com
                                    </li>
                                    <li>
                                        <span class="icon"><i class="fas fa-phone"></i></span>
                                       +91 9846237384
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottombar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-7 d-xl-block d-lg-block d-flex align-items-center">
                                    <div class="logo">
                                        <a href="https://www.sigosoft.co.uk">
                                            <img src="https://www.sigosoft.co.uk/assets/img/logo-sigosoft.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-5 d-xl-none d-lg-none d-block">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-9">
                            <div class="mainmenu">
                                <nav class="navbar navbar-expand-lg">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav mx-auto">
                                            <!--<li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Home
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/partner-with-us">Partner with Us</a>
                                                
                                                </div>
                                            </li>-->
                                            
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        About
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                    <a class="dropdown-item" href="about">Our Profile</a>
                                                    <a class="dropdown-item" href="team">Our Team</a>                                                    
                                                    <a class="dropdown-item" href="technologies">Our Technologies</a>
                                                    <a class="dropdown-item" href="partner-with-us">Partner with Us</a>
                                                
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Services
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/android-app-development-company-in-uk">Android Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/ios-app-development-company-in-uk">iOS Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/cross-platform-app-development-company-in-uk">Cross Platform Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/corporate-website-development-company-in-uk">Corporate Website Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/eCommerce-website-development-company-in-uk">E-Commerce Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/magento-development-company-in-uk">Magento Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/content-management-website-development-company-in-uk">CMS  Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/digital-marketing-company-in-uk">Digital Marketing</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/seo-company-in-uk">Search Engine Optimization</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.co.uk/social-media-marketing-company-in-uk">Social Media Marketing</a>
                                                    
                                                    
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Products
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/loyalty-mobile-app-development-company-in-uk">Loyalty Apps</a>    
                                                <a class="dropdown-item" href="eCommerce-web-and-mobile-apps-development-company-in-uk">E-Commerce Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/supply-chain-mobile-app-development-company-in-uk">Supply Chain Apps</a>
                                                
                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/e-learning-mobile-app-development-company-in-uk">E-Learning Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/community-mobile-app-development-company-in-uk">Community Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/flight-booking-mobile-app-development-company-in-uk">Flight Booking Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/hotel-booking-mobile-app-development-company-in-uk">Hotel Booking Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/online-doctor-consultation-app-development-company-in-uk">Online Consultation Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/gym-mobile-app-development-company-in-uk">Gym & Fitness Apps</a>


                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/rent-a-car-app-development-company-in-uk">Rent a Car Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/tablet-pos-mobile-app-development-company-in-uk">Tablet POS Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/sports-booking-mobile-app-development-company-in-uk">Sports Apps</a>
                                                
                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/food-delivery-app-development-company-in-uk">Food Delivery Apps</a>

                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/classified-app-development-company-in-uk">Classified Apps</a>
                                                
                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/car-wash-app-development-company-in-uk">Car Wash Apps</a>
                                                <a class="dropdown-item" href="https://www.sigosoft.co.uk/van-sales-app-development-company-in-uk">Van sales Apps</a>
                                                </div>
                                            </li>
                                            <?php /*<li class="nav-item">
                                                <a class="nav-link" href="partner-with-us">Partner with Us</a>
                                            </li> */?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.co.uk/portfolio">Portfolio</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.co.uk/blog">Blogs</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.co.uk/career">Careers</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.co.uk/contact">Contact</a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                            <div class="header-buttons">
                                <a href="https://www.sigosoft.co.uk/contact" class="quote-button">Get A Quote</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <style>
 body{
     background:#f2f2f2;
 }
 header{
     background:#fff;
 }
 .list-item{
    background:#fff;
    margin-bottom: 15px;
 }
 #sidebar .widget{
     background:#fff;
    margin-bottom: 15px;
 }
 #home_banner_blog{
     margin-bottom: 15px;
 }
 .container {
    width: 100%;
 }
 .blog {
    padding: 0;
}
                    .header-2 .bottombar .header-buttons {
    width: auto !important;
}
.dmw500{
    width:500px;
}
.banner-2 .banner-content {
    padding: 80px 0;
}
.port{
    background: -webkit-linear-gradient(21deg,#d63438 0,#90191c 100%) !important;
    color: #fff !important;
    height: 50px;
    margin-top: 20px !important;
    padding: 14px !important;
    border-radius: 3px;
}

.header-2 .bottombar .mainmenu .navbar .navbar-nav .nav-item .nav-link.port:before{
      background: #0000;
}
.navbar{
    margin-bottom: 0px;
}
@media only screen and (max-width: 768px){
    .port{
    margin-top: auto !important;
    }
}
                </style>
       <!-- header end -->
       <!-- header end -->