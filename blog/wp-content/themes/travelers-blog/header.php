<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Web & Mobile App Development Service in London, UK | Sigosoft" />
<meta property="og:description" content="Sigosoft is the best mobile app development company in London, UK which follows unique strategies to deliver budget-friendly app development services." />
<meta property="og:url" content="https://www.sigosoft.co.uk/blog" />
<meta property="og:site_name" content="Sigosoft" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@sigosoft_social">
<meta name="twitter:description" content="Sigosoft is the best mobile app development company in London, UK which follows unique strategies to deliver budget-friendly app development services." />
<meta name="twitter:title" content="Web & Mobile App Development Service in London, UK | Sigosoft" />
<meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>
<title>Web & Mobile App Development Service in London, UK | Sigosoft</title>
<meta content="Sigosoft is the best mobile app development company in London, UK which follows unique strategies to deliver budget-friendly app development services." name=description>
<meta content="" name=keywords>
<meta name="robots" content="index, follow">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	    <!-- favicon -->
    <link rel="shortcut icon" href="https://www.sigosoft.co.uk/assets/img/favicon-sigosoft.webp" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="https://www.sigosoft.co.uk/assets/fonts/flaticon.min.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="https://www.sigosoft.co.uk/assets/css/animate.min.css">
    <!-- Owl Carousel -->
    <!-- magnific popup -->
    <link rel="stylesheet" href="https://www.sigosoft.co.uk/assets/css/magnific-popup.min.css">
    <!-- aos scoll animation css -->
    <link rel="stylesheet" href="https://www.sigosoft.co.uk/assets/css/aos.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="https://www.sigosoft.co.uk/assets/css/style.css">
    <!-- responsive -->
    <link rel="stylesheet" href="https://www.sigosoft.co.uk/assets/css/responsive.css">

    <link rel="stylesheet" type="text/css" href="https://www.sigosoft.co.uk/assets/css/custom.css">
    <style>
        body{
            font-family: Montserrat,sans-serif !important;
        }
        h1, h2, h3, h4, h5, h6, a{
    font-family: Montserrat,sans-serif !important;
}
a.tag, .search-form input#searchsubmit{
    background:#d63438 !important;
    font-family: Montserrat,sans-serif !important;
}
    </style>
	<?php wp_head(); ?>
</head>

<?php

$body_id = travelers_blog_get_body_id(); ?>

<body id="<?php echo esc_attr($body_id); ?>" <?php body_class(); ?>>

	<?php 
	do_action( 'travelers_blog_after_body' );

	if ( ! function_exists( 'wp_body_open' ) ) {
        function wp_body_open() {
            do_action( 'wp_body_open' );
        }
	}
	?>

	<header id="masthead">
<?php include "includes/header.php";?>
		
	</header><!-- header section end -->

	<?php 

	global $template; // For elementor
	if( !is_home() && 
		!is_single() && 
		!is_404() && 
		!is_front_page()
	){
		if( basename($template) != 'header-footer.php' ){
			travelers_blog_breadcrumb();
		}
	}