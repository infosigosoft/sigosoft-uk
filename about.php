<!doctype html>
<html lang=en>
<head>
<meta charset=utf-8>
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="website" />
<meta property="og:title" content="About Us | Sigosoft" />
<meta property="og:description" content="Founded in 2014, Sigosoft takes pride in representing our strong company culture. We have successfully developed and deployed hundreds of Mobile Apps all over the world." />
<meta property="og:url" content="https://www.sigosoft.co.uk/about" />
<meta property="og:site_name" content="Sigosoft" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@sigosoft_social">
<meta name="twitter:description" content="Founded in 2014, Sigosoft takes pride in representing our strong company culture. We have successfully developed and deployed hundreds of Mobile Apps all over the world." />
<meta name="twitter:title" content="About Us | Sigosoft" />
<meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>
<title>About Us | Sigosoft</title>
<meta content="Founded in 2014, Sigosoft takes pride in representing our strong company culture. We have successfully developed and deployed hundreds of Mobile Apps all over the world." name=description>
<meta content="" name=keywords>
<meta name="robots" content="index, follow">
    <?php include('styles.php'); ?>

        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">

    </head>
    <body>
        
        <?php include('header.php');?>

        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-about">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>About us</h2>
                            <ul>
                                <li><a href=".">Home</a></li>
                                <li>About</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <!-- about begin -->
        <div class="about-page-about">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="part-text">
                            <h2>Delivering results that endure Technology consultants to the world</h2>
                            <p>Sigosoft Private Limited is a leading software development company specialised in Mobile App Development. It is our pleasure to introduce ourselves as the most trusted Mobile technology brand. A pleasure to share our moto is hundred percent customer satisfaction and reliability. Sigosoft offers creative and technologically advanced solutions for small to large business concerns. We provide the best and most efficient solutions to our clients all over the globe. We have a great team with highly talented and experienced Mobile App Developers, Designers and SEO experts whom are always ready to take any project as a challenge and dedicated themselves to offer the best result. It is our commitment and business policy to produce satisfactory service at a reasonable price to every client. Our major clients located in UAE, Bahrain, Qatar, USA, UK and African countries.</p>
                            <a href="#" class="btn-murtes">Download Company Profile </a>
                        </div>
                    </div>
                    <!--<div class="col-xl-6 col-lg-6 col-md-10">
                        <div class="part-img">
                            <img class="img-1" src="assets/img/about-5.jpg" alt="">
                            <img class="img-2" src="assets/img/about-6.jpg" alt="">
                            <img class="img-3" src="assets/img/about-7.jpg" alt="">
                            <img class="img-4" src="assets/img/about-8.jpg" alt="">
                            <a href="https://www.youtube.com/watch?v=NU9Qyic_mX0" class="play-button mfp-iframe"><i class="fas fa-play"></i></a>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- about end -->

        

        <!-- about-details begin -->
        <div class="about-details">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6 col-md-10">
                        <div class="part-text">
                            <h2 class="first-child">We provide the solutions to grow your business <span class="special">From Good to Great</span>.</h2>
                            <p>Everybody is utilizing portable applications for their business, irrespective of where they remain in their enterprising journey. Building a versatile mobile application isn't only for the enormous aggregates, the little to medium measured organizations are making up for lost time before long! Clearly, portable applications are showing improvement over versatile programs all over the place. 4 Ways your business can profit by having a portable application,</p>

                            <ul>
                                <li><i  class="fas fa-check-square"></i> They’re in all of our pockets now</li>
                                <li> <i class="fas fa-check-square"></i> Provide More Value to Your Customers</li>
                                <li><i class="fas fa-check-square"></i> Build a Stronger Brand</li>
                                <li><i class="fas fa-check-square"></i> Connect Better with Customers</li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-10">
                        <div class="part-img">
                            <img src="assets/img/bg-about4.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- about-details end -->

        <!-- choosing reason begin -->
        
        <div class="choosing-reason-about-page">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="single-reason">
                            <h2><i class="fas fa-desktop"></i></h2>
                            <h3>Digital Solutions</h3>
                            <p>We are the Technology Consultant focused on Software Devlopment, Data Management. We provide solutions for a Dynamic Environment where Business & Technology Strategies Converge.We can ensure you that with us, your apps will be in good hands.</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="single-reason">
                            <h2><i class="fas fa-cogs"></i></h2>
                            <h3>Creative Strategy</h3>
                            <p>How you decide to introduce your brand to the world is one of the most important business decisions you will make. Even after a successful introduction, how you continue to present your brand’s image on an on-going basis is key to its success.</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-12 col-md-12">
                        <div class="single-reason">
                            <h2><i class="far fa-compass"></i></h2>
                            <h3>Powerfull Services</h3>
                            <p>Whether looking to create a mobile application for smartphones, tablets or both, Sigosoft has your organization covered no matter the platform it is to be built on or device it is to be used in conjunction with. We are here to make you a stunning mobile apps.</p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- choosing reason end -->

        <div class="who-we-are">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-8 text-center">
                        <h5>TOGETHER WE REACH THE GOAL</h5>
                        <h2>Who we are</h2>
                        <p>We are a dynamic group of professionals situated in India who can serve globally. Our center is to engage the cutting edge undertakings with cutting edge portable applications.<br/>
                        We are fast, adaptable and concentrated unmistakably on a straightforward plan arranged methodology.</p>
                    </div>
                </div>
            </div>
        </div>

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
    </body>


</html>