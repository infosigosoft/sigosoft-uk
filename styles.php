
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon-sigosoft.webp" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="assets/fonts/flaticon.min.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
    <!-- aos scoll animation css -->
    <link rel="stylesheet" href="assets/css/aos.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- responsive -->
    <link rel="stylesheet" href="assets/css/responsive.css">

    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    
    <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.sigosoft.co.uk\/","name":"Mobile apps development company in UK , London . Best mobile apps development & #1 mobile developers in UK","alternateName":"Best mobile app developers in UK, iphone app developers , iOS app developers in London","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.sigosoft.co.uk\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Organization","url":"https:\/\/www.sigosoft.co.uk\/","sameAs":["https:\/\/www.facebook.com\/sigosoft","https:\/\/twitter.com\/sigosoft_social"],"@id":"#organization","name":"sigosoft","logo":
"https:\/\/www.sigosoft.co.uk\/assets\/img\/logo-sigosoft.png"
}
</script>