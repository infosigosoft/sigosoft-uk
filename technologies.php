<!doctype html>
<html lang=en>
<head>
<meta charset=utf-8>
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Mobile App Technology Consultancy Services UK, London" />
<meta property="og:description" content="."/>
<meta property="og:url" content="https://www.sigosoft.co.uk/technologies" />
<meta property="og:site_name" content="Sigosoft" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@sigosoft_social">
<meta name="twitter:description" content="Mobile App Development Consultancy Company in the UK, London that uses the latest technologies and tools to offer cutting edge solutions to our clients."/>
<meta name="twitter:title" content="Mobile App Technology Consultancy Services UK, London." />
<meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>
<title>Mobile App Technology Consultancy Services UK, London</title>
<meta content="Mobile App Development Consultancy Company in the UK, London that uses the latest technologies and tools to offer cutting edge solutions to our clients" name=description>
<meta content="" name=keywords>
<meta name="robots" content="index, follow">
    <?php include('styles.php'); ?>
        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">
    </head>
    <body>

        <?php include('header.php'); ?>

        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-technologies">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>Mobile App Technology Consultancy Services in UK, London</h2>
                            <ul>
                                <li><a href=".">Home</a></li>
                                <li>Technologies</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <!-- team begin -->
        <div class="technologies-page">
            <div class="container">
                <div class="technologies">
                    <div class="row">

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/html.jpg" alt="Html 5">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/css.jpg" alt="CSS">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/javascript.jpg" alt="Javascript">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/bootstrap.jpg" alt="Bootstrap">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/react-native.jpg" alt="React Native">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/python.jpg" alt="Python">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/java.jpg" alt="Java">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/angular-js.jpg" alt="Angular JS">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/php.jpg" alt="PHP">
                            </div>
                        </div> 


                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/codeigniter.jpg" alt="Codeigniter">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/laravel.jpg" alt="Laravel">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/yii.jpg" alt="Yii2">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/swift.jpg" alt="Swift">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/firebase.jpg" alt="Firebase">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/wordpress.jpg" alt="Wordpress">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/woocommerce.jpg" alt="Woocommerce">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/magento.jpg" alt="Magento">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/google-cloud.jpg" alt="Google Cloud">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/amazon-webservices.jpg" alt="Amazon Webservices">
                            </div>
                        </div> 

                        <div class="col-sm-3 col-6">
                            <div class="single-technology">
                                <img src="assets/img/technologies/mysql.jpg" alt="MySQL">
                            </div>
                        </div> 

                    </div>
                </div>
            </div>
        </div>
        <!-- team end -->

        

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>

        
        <script src="assets/js/banner-slide-active.js"></script>
        
        <!-- filterizr js -->
        <script src="assets/js/jquery.filterizr.min.js"></script>
        
    </body>


</html>