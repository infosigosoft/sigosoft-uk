        <!-- footer begin -->
        <div class="footer footer-2">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between">
                    
                    <div class="col-xl-2 col-lg-2 col-md-6">
                        <div class="links-widget">
                            <h3>About Us</h3>
                            <ul>
                                <li>
                                    <a href="about">Our Profile</a>
                                </li>
                                <li>
                                    <a href="partner-with-us">Partner with Us</a>
                                </li>
                                <li>
                                    <a href="team">Our Team</a>
                                </li>
                                <li>
                                    <a href="careers">Careers</a>
                                </li>
                                
                                <li>
                                    <a href="technologies">Technologies</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="links-widget">
                            <h3>Services</h3>
                            <ul>
                                <li>
                                    <a href="android-app-development-company-in-uk">Android Development</a>
                                </li>
                                <li>
                                    <a href="ios-app-development-company-in-uk">iOS Development</a>
                                </li>
                                <li>
                                    <a href="Flutter-app-development-company-in-uk">Cross Platform Apps</a>
                                </li>
                                <li>
                                    <a href="eCommerce-website-development-company-in-uk">E-Commerce Development</a>
                                </li>
                                <li>
                                    <a href="magento-development-company-in-uk">Magento Development</a>
                                </li>
                                
                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-6">
                        <div class="links-widget">
                            <h3>Products</h3>
                            <ul>
                                <li>
                                    <a href="eCommerce-web-and-mobile-apps-development-company-in-uk">E-Commerce Apps</a>
                                </li>
                                <li>
                                    <a href="supply-chain-mobile-app-development-company-in-uk">Supply Chain Apps</a>
                                </li>
                                <li>
                                    <a href="e-learning-mobile-app-development-company-in-uk">E-Learning Apps</a>
                                </li>
                                <li>
                                    <a href="telemedicine-mobile-app-development-company-in-uk">Telemedicine Apps</a>
                                </li>
                                <li>
                                    <a href="loyalty-mobile-app-development-company-in-uk">Loyalty Apps</a>
                                </li>
                                <li>
                                    <a href="flight-booking-mobile-app-development-company-in-uk">Flight Booking Apps</a>
                                </li>
                                <li>
                                    <a href="hotel-booking-mobile-app-development-company-in-uk">Hotel Booking Apps</a>
                                </li>

                                <li>
                                    <a href="food-delivery-app-development-company-in-uk">Food Delivery Apps</a>
                                </li>
                                <li>
                                    <a href="van-sales-app-development-company-in-uk">Van Sales Apps</a>
                                </li>
                                <li>
                                    <a href="classified-app-development-company-in-uk">Classified Apps</a>
                                </li>
                                <li>
                                    <a href="gym-mobile-app-development-company-in-uk">Gym Fitness Apps</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="about-widget links-widget">
                            <h3>Contact</h3>
                            <ul>
                                 <li><i class="fas fa-phone"></i> <a href="tel:+91 4952433123">+91 4952433123</a></li>
                                <li><i class="fas fa-mobile-alt"></i> <a href="tel:+971 56 253 0256">+91 9846237384</a></li>
                                <?php /*<li><a href="https://api.whatsapp.com/send?phone=919846237384"><i class="fab fa-whatsapp"></i> +91 9846237384</a></li> */?>
                                <li><i class="far fa-envelope-open"></i> <a href="mailto:info@sigosoft.com">info@sigosoft.com</a></li>
                                <li style="color: #bdbdbd; font-size: 15px; line-height: 25px;"><i class="fas fa-map-marker-alt"></i>
                                    <span>36, Briarway, <br>Fishponds, Bristol, <br>BS16 4EB, <br>United Kingdom</span><br>
                                    
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- footer end -->
        
        

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="badges-wrapper text-center">
                            <ul class="badges">
                                <li><a href="https://topsoftwarecompanies.co/united-arab-emirates/app-development/agencies/dubai" target="_blank"><img src="assets/img/badges/1.png" alt="software developers bristol"></a></li>
                                <li><img src="assets/img/badges/2.png" alt=""></li>
                                <li><img src="assets/img/badges/3.png" alt=""></li>
                                <li><img src="assets/img/badges/4.png" alt=""></li>
                                <li><img src="assets/img/badges/5.png" alt=""></li>
                                <li><img src="assets/img/badges/6.png" alt=""></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>







        <!-- copyright begin -->
        <div class="copyright">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="cp-area">
                            <p>Copyright © 2014-2020 Sigosoft. All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="social-area">
                            <ul>
                               <li>
<a class="facebook" href= https://www.facebook.com/sigosoft><i class="fab fa-facebook-f"></i></a>
</li>
<li>
<a class="instagram" href=https://www.instagram.com/sigosoft_><i class="fab fa-instagram"></i></a>
</li>
<li>
<a class="twitter" href="https://twitter.com/sigosoft_social"><i class="fab fa-twitter"></i></a>
</li>
<li>
<a class="skype" href=https://join.skype.com/invite/Qq1GmNOs9DDV><i class="fab fa-skype"></i></a>
</li>
<li>
<a class="facebook" href= https://www.linkedin.com/company/sigosoft/><i class="fab fa-linkedin"></i></a>

</li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        <!-- copyright end -->

        <!-- copyright end -->
        <div class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=919846237384" target="_blank"><img src="assets/img/whatsapp.png" alt="whatsapp"/></a>
        </div>
        
        
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5f022eb4760b2b560e6fc8b8/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->